var express = require('express');
var router = express.Router();

const journeyPlan = require('../controllers/journeyPlan')
const passport = require('passport');


router.get('/', passport.authenticate('jwt', { 'session': false }), journeyPlan.getAllJourneyPlans);

router.get('/journeyPlan/:id', passport.authenticate('jwt', { 'session': false }), journeyPlan.getJourneyPlanById);

router.post('/journeyPlan', passport.authenticate('jwt', { 'session': false }), journeyPlan.createJourneyPlan);

router.put('/journeyPlan', passport.authenticate('jwt', { 'session': false }), journeyPlan.updateJourneyPlan);

router.delete('/journeyPlan', passport.authenticate('jwt', { 'session': false }), journeyPlan.deleteJourneyPlan);

module.exports = router;
