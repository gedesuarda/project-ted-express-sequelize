var express = require('express');
var router = express.Router();
const passport = require('passport');


const User = require('../controllers/user')

router.get('/', passport.authenticate('jwt', { 'session': false }), User.getAllUsers);

router.get('/user/:id', passport.authenticate('jwt', { 'session': false }), User.getUserById);

router.post('/user', User.createUser);

router.put('/user', passport.authenticate('jwt', { 'session': false }), User.updateUser);

router.delete('/user', passport.authenticate('jwt', { 'session': false }), User.deleteUser);

module.exports = router;