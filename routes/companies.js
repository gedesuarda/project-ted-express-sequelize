var express = require('express');
var router = express.Router();
const company = require('../controllers/company')
const passport = require('passport');

router.get('/', passport.authenticate('jwt', { 'session': false }), company.getAllCompanies);

router.get('/company/:id', passport.authenticate('jwt', { 'session': false }), company.getCompanyById);

router.post('/company', passport.authenticate('jwt', { 'session': false }), company.createCompany);

router.put('/company', passport.authenticate('jwt', { 'session': false }), company.updateCompany);

router.delete('/company', passport.authenticate('jwt', { 'session': false }), company.deleteCompany);

module.exports = router;
