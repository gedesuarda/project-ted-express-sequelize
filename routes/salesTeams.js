var express = require('express');
var router = express.Router();

const SalesTeam = require('../controllers/salesTeam.js')
const passport = require('passport');


router.get('/', passport.authenticate('jwt', { 'session': false }),SalesTeam.getAllSalesTeams);

router.get('/salesTeam/:id', passport.authenticate('jwt', { 'session': false }), SalesTeam.getSalesTeamById);

router.post('/salesTeam', passport.authenticate('jwt', { 'session': false }), SalesTeam.createSalesTeam);

router.put('/salesTeam', passport.authenticate('jwt', { 'session': false }), SalesTeam.updateSalesTeam);

router.delete('/salesTeam', passport.authenticate('jwt', { 'session': false }), SalesTeam.deleteSalesTeam);

module.exports = router;
