const passport = require("passport");
const passportJWT = require("passport-jwt");
const { User } = require('../models')

const JwtStrategy = passportJWT.Strategy;
const ExtractJwt = passportJWT.ExtractJwt;
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');

passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
},
    function (email, password, next) {
        const saltRounds = 10;
        const salt = bcrypt.genSaltSync(saltRounds);
        const passwordHashed = bcrypt.hashSync(password, salt);

        return User.findOne(
            {
                attributes: {
                    exclude: ['createdAt', 'updatedAt']
                },
                where: { email: email }

            })
            .then(user => {
                if (!user) {
                    return next(null, false, { message: 'Incorrect email or password.' });
                }

                hash = user.password
                bcrypt.compare(password, hash).then(function (result) {
                    if (!result) {
                        return next(null, false, { message: 'Incorrect email or password.' });
                    }

                    let userWithoutPassword = user.get();
                    delete userWithoutPassword['password']
                    return next(null, userWithoutPassword, {
                        message: 'Logged In Successfully'
                    });
                });
            })
            .catch(err => {
                return next(err);
            });
    }
));

passport.use(
    new JwtStrategy(
        {
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: '12345',
        },
        function (jwtPayload, next) {
            return User.findOne(
                {
                    where: {id: jwtPayload.id},
                    attributes: {
                        exclude: ['createdAt', 'updatedAt', 'password']
                    },

                }
                )
                .then(user => {
                    return next(null, user);
                })
                .catch(err => {
                    return next(err);
                });
        }
    )
);
