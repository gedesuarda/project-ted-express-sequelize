FROM keymetrics/pm2:10-alpine

RUN apk update && apk upgrade && \
  apk add --no-cache \
  bash \
  git \
  curl \
  openssh

MAINTAINER sugeng dan nanda

RUN mkdir -p /usr/src/project-ted-express-sequelize
WORKDIR /usr/src/project-ted-express-sequelize

COPY package*.json ./
RUN npm cache clean --force
RUN npm install
COPY . .

EXPOSE 3000

CMD [ "pm2-runtime", "start", "pm2.json", "--env", "production" ]
