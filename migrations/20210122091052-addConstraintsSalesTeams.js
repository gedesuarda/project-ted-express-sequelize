'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.addConstraint('SalesTeams', {
      fields: ['companyId'],
      type: 'foreign key',
      name: 'salesteam_company_constraint',
      references: {
        table: 'Companies',
        field: 'id'
      },
    });

    queryInterface.addConstraint('SalesTeams', {
      fields: ['journeyPlanId'],
      type: 'foreign key',
      name: 'salesteam_journey_consts',
      references: {
        table: 'JourneyPlans',
        field: 'id'
      },
    });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
