'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

    return Promise.all([
      queryInterface.addColumn(
        'SalesTeams',
        'companyId',
        {
          type: Sequelize.INTEGER
        }
      ),
      queryInterface.addColumn(
        'SalesTeams',
        'journeyPlanId',
        {
          type: Sequelize.INTEGER
        }
      ),
    ]);

  },

  down: async (queryInterface, Sequelize) => {

  }
};
