'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class JourneyPlans extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
     // JourneyPlans.belongsTo(models.SalesTeams, {foreignKey: 'sfName', as: 'JourneyOutlets'})
      // define association here

      JourneyPlans.belongsToMany(models.Companies, { through: 'SalesTeams', foreignKey: 'journeyPlanId' })
    }
  };
  JourneyPlans.init({
    sfName: DataTypes.STRING,
    journeyOutlet: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'JourneyPlans',
  });
  return JourneyPlans;
};