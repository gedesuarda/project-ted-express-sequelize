'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class SalesTeams extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      //   SalesTeams.hasMany(models.JourneyPlans, {foreignKey: 'sfName',as: 'JourneyOutlets'})


      //User.belongsToMany(models.WorkingDay, {through: 'UsersWorkingDays', foreignKey: 'userId', as: 'days'})
     // SalesTeams.belongsToMany(models.Companies, { through: 'Companies', foreignKey: 'companyId' })
      //SalesTeams.belongsToMany(models.JourneyPlans, { through: 'JourneyPlans', foreignKey: 'journeyPlanId' })
    }
  };
  SalesTeams.init({
    sfName: DataTypes.STRING,
    teritory: DataTypes.STRING,
    division: DataTypes.STRING,
    companyId: DataTypes.INTEGER,
    journeyPlanId: DataTypes.INTEGER,

  }, {
    sequelize,
    modelName: 'SalesTeams',
  });
  return SalesTeams;
};