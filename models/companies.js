'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Companies extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      //Companies.hasMany(models.SalesTeams, {foreignKey: 'sfName',as: 'SF'})
      
      // define association here
      Companies.belongsToMany(models.JourneyPlans, { through: 'SalesTeams', foreignKey: 'companyId' })
    }
  };
  Companies.init({
    subBO: DataTypes.STRING,
    teritory: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Companies',
  });
  return Companies;
};