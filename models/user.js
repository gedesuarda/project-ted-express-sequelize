'use strict';

const bcrypt = require('bcrypt');

const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
  
    static associate(models) {
    }
  };
  User.init({
    name: DataTypes.STRING,
    password: {
      type: DataTypes.STRING,

      set(password) {
        const saltRounds = 10;

        const salt = bcrypt.genSaltSync(saltRounds);
        const passwordHashed = bcrypt.hashSync(password, salt);
        this.setDataValue('password', passwordHashed);
      }

    },
    email: DataTypes.STRING,
    phone_number: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};