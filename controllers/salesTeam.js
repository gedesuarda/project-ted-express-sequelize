const { SalesTeams } = require('../models')
const Sequelize = require('sequelize')
const Op = Sequelize.Op;
const { paginate, getPageAndPageSize } = require('../utils/pagination')
const responses = require('../common/api_response')

function getAllSalesTeams(req, res, next) {

    let p = getPageAndPageSize(req);

    SalesTeams.findAll(
        paginate({

        }, {
            page: p.page,
            pageSize: p.pageSize
        })

    ).then(function (results) {
        res.status(200).send(responses._200({
            status: 'success',
            message: 'success',
            data: results
        }))

    }).catch(function (error) {
        res.status(405).send(responses._405(
            {
                status: 'error',
                message: error,
                data: {},
            }))
    });
}

function createSalesTeam(req, res, next) {
    SalesTeams.create({
        journeyPlanId: req.body.journeyPlanId,
        companyId: req.body.companyId,
        sfName: req.body.sfName,
        teritory: req.body.teritory,
        division: req.body.division,
    }).then(function (salesTeam) {
        if (salesTeam) {
            res.status(201).send(responses._201(
                {
                    status: 'success',
                    message: 'success',
                    data: salesTeam
                }))
        } else {

            res.status(422).send(responses._422(
                {
                    status: 'error',
                    message: 'Error inserting new record',
                    data: {}
                }))
        }
    }).catch(function (error) {
        res.status(405).send(responses._405(
            {
                status: 'error',
                message: error,
                data: {}
            }))
    })
        ;
}

function getSalesTeamById(req, res, next) {
    SalesTeams.findOne({ where: { id: req.params.id } }).then(function (result) {
        if (!result) {
            res.status(422).send(responses._422(
                {
                    status: 'error',
                    message: 'Company not found',
                    data: {}
                }))
        }
        else {
            res.status(200).send(responses._200(
                {
                    status: 'success',
                    message: 'success',
                    data: result,
                }))
        }
    }).catch(function (error) {
        res.status(405).send(responses._405(
            {
                status: 'error',
                message: error,
                data: {}

            }))
    });
}

function updateSalesTeam(req, res, next) {
    SalesTeams.update(
        {
            journeyPlanId: req.body.journeyPlanId,
            companyId: req.body.companyId,
            sfName: req.body.sfName,
            teritory: req.body.teritory,
            division: req.body.division, 
        },
        { where: { id: req.body.id } }
    )
        .then(result => {
            res.status(201).send(responses._201(
                {
                    status: 'success',
                    message: 'success',
                    data: result
                }))
        }
        )
        .catch(error => {
            res.status(405).send(responses._405(
                {
                    status: 'error',
                    message: error,
                    data: {}
                }))
        }
        )
}

function deleteSalesTeam(req, res, next) {
    SalesTeams.destroy(
        { where: { id: req.body.id } }
    )
        .then(result => {
            res.status(201).send(responses._201(
                {
                    status: 'success',
                    message: 'success',
                    data: result
                }))
        }
        )
        .catch(err => {
            {
                if (err.name == "SequelizeForeignKeyConstraintError") {
                    res.status(422).send(responses._422(
                        {
                            status: 'error',
                            message: 'Sales team is being used',
                            data: {}
                        }))
                }
                else {
                    res.status(405).send(responses._405(
                        {
                            status: 'error',
                            message: error,
                            data: {}
                        }))

                }
            }
        }


        )
}


module.exports = {
    getAllSalesTeams,
    getSalesTeamById,
    createSalesTeam,
    updateSalesTeam,
    deleteSalesTeam,
}