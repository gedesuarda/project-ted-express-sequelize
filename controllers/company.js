const { Companies, JourneyPlans, SalesTeams } = require('../models')
const Sequelize = require('sequelize')
const Op = Sequelize.Op;
const { paginate, getPageAndPageSize } = require('../utils/pagination')
const responses = require('../common/api_response');


function getAllCompanies(req, res, next) {

    let p = getPageAndPageSize(req);

    Companies.findAll(
        paginate(
            {
                attributes: {
                    exclude: ['createdAt','updatedAt']
                },
                include: [{
                    model: JourneyPlans,
                    as: 'JourneyPlans',
                    required: false,
                    attributes: ['id', 'sfName','journeyOutlet'],
                    through: {
                        model: SalesTeams,
                        as: 'SalesTeams',
                        attributes: ['journeyPlanId', 'sfName','teritory', 'division' , 'companyId'],
                    }
                }]

            },
            {
                page: p.page,
                pageSize: p.pageSize
            })



    ).then(function (results) {
        res.status(200).send(responses._200({
            status: 'success',
            message: 'success',
            data: results
        }))

    }).catch(function (error) {
        res.status(405).send(responses._405(
            {
                status: 'error',
                message: error,
                data: {},
            }))
    });
}

function getCompanyById(req, res, next) {
    Companies.findOne(
        { 
            attributes: {
                exclude: ['createdAt','updatedAt']
            },
            include: [{
                model: JourneyPlans,
                as: 'JourneyPlans',
                required: false,
                attributes: ['id', 'sfName','journeyOutlet'],
                through: {
                    model: SalesTeams,
                    as: 'SalesTeams',
                    attributes: ['journeyPlanId', 'sfName','teritory', 'division' , 'companyId'],
                }
            }],

            where: { id: req.params.id } }
        ).then(function (result) {
        if (!result) {
            res.status(422).send(responses._422(
                {
                    status: 'error',
                    message: 'Company not found',
                    data: {}
                }))
        }
        else {
            res.status(200).send(responses._200(
                {
                    status: 'success',
                    message: result,
                    data: {},
                }))
        }
    }).catch(function (error) {
        res.status(405).send(responses._405(
            {
                status: 'error',
                message: error,
                data: {}

            }))
    });
}

function getCompaniesByName(req, res, next) {
    let p = getPageAndPageSize(req);

    Companies.findAll(
        paginate(
            {
                attributes: {
                    exclude: ['createdAt','updatedAt']
                },
                include: [{
                    model: JourneyPlans,
                    as: 'JourneyPlans',
                    required: false,
                    attributes: ['id', 'sfName','journeyOutlet'],
                    through: {
                        model: SalesTeams,
                        as: 'SalesTeams',
                        attributes: ['journeyPlanId', 'sfName','teritory', 'division' , 'companyId'],
                    }
                }],
                where:
                {
                    company: {
                        [Op.iLike]: '%' + req.query.name + '%'
                    }
                }
            },
            {
                page: p.page,
                pageSize: p.pageSize
            }
        )
    ).then(function (results) {
        if (!results) {
            res.status(422).send(responses._422(
                {
                    status: 'error',
                    message: 'Company not found',
                    data: {}
                }))
        }
        else {
            res.status(200).send(responses._200(
                {
                    status: 'success',
                    message: 'success',
                    data: results
                }))
        }

    }).catch(function (error) {
        res.status(405).send(responses._405(
            {
                status: 'error',
                message: error,
                data: {}
            }))
    });
}

function createCompany(req, res, next) {
    Companies.create({
        subBO: req.body.subBO,
        teritory: req.body.teritory,
    }).then(function (companyName) {
        if (companyName) {
            res.status(201).send(responses._201(
                {
                    status: 'success',
                    message: 'success',
                    data: companyName
                }))
        } else {

            res.status(422).send(responses._422(
                {
                    status: 'error',
                    message: 'Error inserting new record',
                    data: {}
                }))
        }
    }).catch(function (error) {
        res.status(405).send(responses._405(
            {
                status: 'error',
                message: error,
                data: {}
            }))
    })
        ;
}

function updateCompany(req, res, next) {
    Companies.update(
        {
            subBO: req.body.subBO,
            teritory: req.body.teritory,
        },
        { where: { id: req.body.id } }
    )
        .then(result => {
            res.status(201).send(responses._201(
                {
                    status: 'success',
                    message: 'success',
                    data: result
                }))
        }
        )
        .catch(error => {
            res.status(405).send(responses._405(
                {
                    status: 'error',
                    message: error,
                    data: {}
                }))
        }
        )
}

function deleteCompany(req, res, next) {
    Companies.destroy(
        { where: { id: req.body.id } }
    )
        .then(result => {
            res.status(201).send(responses._201(
                {
                    status: 'success',
                    message: 'success',
                    data: result
                }))
        }
        )
        .catch(error => {
            {
                if (error.name == "SequelizeForeignKeyConstraintError") {
                    res.status(422).send(responses._422(
                        {
                            status: 'error',
                            message: 'Company is used by the Sales',
                            data: {}
                        }))
                }
                else {
                    res.status(405).send(responses._405(
                        {
                            status: 'error',
                            message: error,
                            data: {}
                        }))

                }
            }
        }


        )
}


module.exports = {
    getAllCompanies,
    getCompanyById,
    createCompany,
    updateCompany,
    deleteCompany,
    getCompaniesByName
}