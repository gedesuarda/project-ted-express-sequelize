const jwt = require('jsonwebtoken');
const passport = require('passport');


function login(req, res, next) {
    passport.authenticate('local', { session: false }, (err, user, info) => {
        if (err || !user) {
            return res.status(400).json({
                message: info ? info.message : 'Login failed',
                user: user
            });
        }


        const token = jwt.sign(JSON.stringify(user), '12345');

        return res.json({ user, token });

        // req.login(user, { session: false }, (err) => {
        //     if (err) {
        //         res.send(err);
        //     }


        // });
    })
        (req, res);

}

module.exports = {
    login
}