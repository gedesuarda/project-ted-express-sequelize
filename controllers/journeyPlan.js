const { JourneyPlans, Companies, SalesTeams } = require('../models')
const Sequelize = require('sequelize')
const Op = Sequelize.Op;
const { paginate, getPageAndPageSize } = require('../utils/pagination')
const responses = require('../common/api_response')

function getAllJourneyPlans(req, res, next) {

    let p = getPageAndPageSize(req);

    JourneyPlans.findAll(
        paginate(
            {
                attributes: {
                    exclude: ['createdAt', 'updatedAt']
                },
                include: [{
                    model: Companies,
                    as: 'Companies',
                    required: false,
                    attributes: ['id', 'subBO', 'teritory'],
                    through: {
                        model: SalesTeams,
                        as: 'SalesTeams',
                        attributes: ['journeyPlanId', 'sfName', 'teritory', 'division', 'companyId'],
                    }
                }],
            },
            {
                page: p.page,
                pageSize: p.pageSize
            })

    ).then(function (results) {
        res.status(200).send(responses._200({
            status: 'success',
            message: 'success',
            data: results
        }))

    }).catch(function (error) {
        res.status(405).send(responses._405(
            {
                status: 'error',
                message: error,
                data: {},
            }))
    });
}

function getJourneyPlanById(req, res, next) {
    JourneyPlans.findOne(
        {
            attributes: {
                exclude: ['createdAt', 'updatedAt']
            },
            include: [{
                model: Companies,
                as: 'Companies',
                required: false,
                attributes: ['id', 'subBO', 'teritory'],
                through: {
                    model: SalesTeams,
                    as: 'SalesTeams',
                    attributes: ['journeyPlanId', 'sfName', 'teritory', 'division', 'companyId'],
                }
            }],
            where: { id: req.params.id }
        }).then(function (result) {
            if (!result) {
                res.status(422).send(responses._422(
                    {
                        status: 'error',
                        message: 'Journey Plan not found',
                        data: {}
                    }))
            }
            else {
                res.status(200).send(responses._200(
                    {
                        status: 'success',
                        message: 'success',
                        data: result,
                    }))
            }
        }).catch(function (error) {
            res.status(405).send(responses._405(
                {
                    status: 'error',
                    message: error,
                    data: {}

                }))
        });
}

function getJourneyPlansByName(req, res, next) {
    let p = getPageAndPageSize(req);

    JourneyPlans.findAll(
        paginate(
            {
                attributes: {
                    exclude: ['createdAt', 'updatedAt']
                },
                include: [{
                    model: Companies,
                    as: 'Companies',
                    required: false,
                    attributes: ['id', 'subBO', 'teritory'],
                    through: {
                        model: SalesTeams,
                        as: 'SalesTeams',
                        attributes: ['journeyPlanId', 'sfName', 'teritory', 'division', 'companyId'],
                    }
                }],
            },

            {
                where:
                {
                    sfName: {
                        [Op.iLike]: '%' + req.query.sfName + '%'
                    }
                }
            },
            {
                page: p.page,
                pageSize: p.pageSize
            }
        )
    ).then(function (results) {
        if (!results) {
            res.status(422).send(responses._422(
                {
                    status: 'error',
                    message: 'Journey Plan not found',
                    data: {}
                }))
        }
        else {
            res.status(200).send(responses._200(
                {
                    status: 'success',
                    message: 'success',
                    data: results
                }))
        }

    }).catch(function (error) {
        res.status(405).send(responses._405(
            {
                status: 'error',
                message: error,
                data: {}
            }))
    });
}

function createJourneyPlan(req, res, next) {
    JourneyPlans.create({
        sfName: req.body.sfName,
        journeyOutlet: req.body.journeyOutlet,
    }).then(function (journeyPlan) {
        if (journeyPlan) {
            res.status(201).send(responses._201(
                {
                    status: 'success',
                    message: 'success',
                    data: journeyPlan
                }))
        } else {

            res.status(422).send(responses._422(
                {
                    status: 'error',
                    message: 'Error inserting new record',
                    data: {}
                }))
        }
    }).catch(function (error) {
        res.status(405).send(responses._405(
            {
                status: 'error',
                message: error,
                data: {}
            }))
    })
        ;
}

function updateJourneyPlan(req, res, next) {
    JourneyPlans.update(
        {
            sfName: req.body.sfName,
            journeyOutlet: req.body.journeyOutlet,
        },
        { where: { id: req.body.id } }
    )
        .then(result => {
            res.status(201).send(responses._201(
                {
                    status: 'success',
                    message: 'success',
                    data: result
                }))
        }
        )
        .catch(error => {
            res.status(405).send(responses._405(
                {
                    status: 'error',
                    message: error,
                    data: {}
                }))
        }
        )
}

function deleteJourneyPlan(req, res, next) {
    JourneyPlans.destroy(
        { where: { id: req.body.id } }
    )
        .then(result => {
            res.status(201).send(responses._201(
                {
                    status: 'success',
                    message: 'success',
                    data: result
                }))
        }
        )
        .catch(error => {
            {
                if (error.name == "SequelizeForeignKeyConstraintError") {
                    res.status(422).send(responses._422(
                        {
                            status: 'error',
                            message: 'Journey plan is being used',
                            data: {}
                        }))
                }
                else {
                    res.status(405).send(responses._405(
                        {
                            status: 'error',
                            message: error,
                            data: {}
                        }))

                }
            }
        }


        )
}


module.exports = {
    getAllJourneyPlans,
    getJourneyPlanById,
    createJourneyPlan,
    updateJourneyPlan,
    deleteJourneyPlan,
    getJourneyPlansByName
}