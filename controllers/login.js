const dataUser = require('../models').User
const JWT = require("jsonwebtoken");
const bcrypt = require('bcrypt');
require("dotenv/config");


module.exports = {
  async login(req, res) {
    try {
      const email = req.body.email;
      const password = req.body.password;
      const user = await dataUser.findOne({
        where: {
          email: email
        }
      })

      if (email === null) {
        res.status(400).send({
          message: "your email is wrong",
        })
      }
      const passwordUser = user.password;
      const comparePassword = bcrypt.compareSync(password, passwordUser);
      if (!comparePassword) {
        res.status(401).send({
          message: "you failed login"
        })
      }

      const token = JWT.sign(
        { delete: {user:password} },
        process.env.SECRETKEY
      );

      res.status(200).send({
        message: "your email is right",
        token
      });

    } catch (error) {
      console.log('error', error)
      res.status(500).send({
        message: "email salah"
      })
    }
  }
}