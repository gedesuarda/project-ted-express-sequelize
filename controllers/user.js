const { User } = require('../models')
const Sequelize = require('sequelize')
const Op = Sequelize.Op;
const { paginate, getPageAndPageSize } = require('../utils/pagination')
const responses = require('../common/api_response')

function getAllUsers(req, res, next) {

    let p = getPageAndPageSize(req);

    User.findAll(
        paginate({
            attributes: {exclude: ['password']},

        }, {
            page: p.page,
            pageSize: p.pageSize
        }),
        

    ).then(function (results) {
        res.status(200).send(responses._200({
            status: 'success',
            message: 'success',
            data: results
        }))

    }).catch(function (error) {
        res.status(405).send(responses._405(
            {
                status: 'error',
                message: error,
                data: {},
            }))
    });
}

function getUserById(req, res, next) {
    User.findOne({ where: { id: req.params.id },
        attributes: {exclude: ['password']}, } ).then(function (result) {
        if (!result) {
            res.status(422).send(responses._422(
                {
                    status: 'error',
                    message: 'User not found',
                    data: {}
                }))
        }
        else {
            res.status(405).send(responses._405(
                {
                    status: 'error',
                    message: error,
                    data: {},
                }))
        }
    }).catch(function (error) {
        res.status(405).send(responses._405(
            {
                status: 'error',
                message: error,
                data: {}

            }))
    });
}

function getUsersByName(req, res, next) {
    let p = getPageAndPageSize(req);

    User.findAll(
        paginate(
            {
                attributes: {exclude: ['password']},
                where:
                {
                    name: {
                        [Op.iLike]: '%' + req.query.name + '%'
                    }
                }
            },
            {
                page: p.page,
                pageSize: p.pageSize
            }
        )
    ).then(function (results) {
        if (!results) {
            res.status(422).send(responses._422(
                {
                    status: 'error',
                    message: 'User not found',
                    data: {}
                }))
        }
        else {
            res.status(200).send(responses._200(
                {
                    status: 'success',
                    message: 'success',
                    data: results
                }))
        }

    }).catch(function (error) {
        res.status(405).send(responses._405(
            {
                status: 'error',
                message: error,
                data: {}
            }))
    });
}

function createUser(req, res, next) {
    User.create({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
    }).then(function (user) {
        if (user) {
            res.status(201).send(responses._201(
                {
                    status: 'success',
                    message: 'success',
                    data: user
                }))
        } else {

            res.status(422).send(responses._422(
                {
                    status: 'error',
                    message: 'Error inserting new record',
                    data: {}
                }))
        }
    }).catch(function (error) {
        res.status(405).send(responses._405(
            {
                status: 'error',
                message: error,
                data: {}
            }))
    })
        ;
}

function updateUser(req, res, next) {
    User.update(
        {
            name: req.body.name,
            email: req.body.email,
            password: req.body.password,
        },
        { where: { id: req.body.id } }
    )
        .then(result => {
            res.status(201).send(responses._201(
                {
                    status: 'success',
                    message: 'success',
                    data: result
                }))
        }
        )
        .catch(error => {
            res.status(405).send(responses._405(
                {
                    status: 'error',
                    message: error,
                    data: {}
                }))
        }
        )
}

function deleteUser(req, res, next) {
    User.destroy(
        { where: { id: req.body.id } }
    )
        .then(result => {
            res.status(201).send(responses._201(
                {
                    status: 'success',
                    message: 'success',
                    data: result
                }))
        }
        )
        .catch(err => {
            {
                if (err.name == "SequelizeForeignKeyConstraintError") {
                    res.status(422).send(responses._422(
                        {
                            status: 'error',
                            message: 'Company is used by the Sales',
                            data: {}
                        }))
                }
                else {
                    res.status(405).send(responses._405(
                        {
                            status: 'error',
                            message: error,
                            data: {}
                        }))

                }
            }
        }


        )
}


module.exports = {
    getAllUsers,
    getUserById,
    createUser,
    updateUser,
    deleteUser,
    getUsersByName
}