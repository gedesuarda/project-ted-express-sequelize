const paginate = (query, { page, pageSize }) => {
    const offset = (page - 1) * pageSize;
    const limit = pageSize;
  
    return {
      ...query,
      offset,
      limit,
    };
  };
  
  const getPageAndPageSize = (req) => {
    let page = 1;
    let pageSize = 10;
    if (req.query.page) {
      page = req.query.page
      pageSize = req.query.pageSize
    }
    if (req.query.pageSize) {
      pageSize = req.query.pageSize
    }
    return {
      page, pageSize
    }
  };
  
  
  module.exports = {
    paginate,
    getPageAndPageSize
  }