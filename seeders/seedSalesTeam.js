'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('SalesTeams',
      [
        {
          sfName: 'Angga',
          teritory: 'Kecamatan Glagah',
          division: 'TDC Banyuwangi',
          updatedAt: new Date (),
          createdAt: new Date ()
        }
      ],
    {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('SalesTeams', null, {});
  }
};
