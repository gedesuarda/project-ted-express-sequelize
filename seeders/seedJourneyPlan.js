'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('JourneyPlans',
      [
        {
          sfName: 'Angga',
          journeyOutlet: 70,
          updatedAt: new Date (),
          createdAt: new Date ()
        }
      ],
    {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('JourneyPlans', null, {});
  }
};
