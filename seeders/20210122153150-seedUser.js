'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Users',
      [
        {
          id: '1',
          name: 'joko',
          password: 'asd123',
          email: 'joko@telkomsel',
          phone_number: '08123456789',
          updatedAt: new Date (),
          createdAt: new Date ()
        }
      ],
    {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('SalesTeams', null, {});
  }
};
