'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Companies',
      [
        {
          subBO: 'SBO Banyuwangi',
          teritory: 'Kabupaten Banyuwangi',
          updatedAt: new Date (),
          createdAt: new Date ()
          
        },
        {
          subBO: 'SBO Bondowoso',
          teritory: 'Kabupaten Bondowoso',
          updatedAt: new Date (),
          createdAt: new Date ()
          
        }
      ],
    {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Companies', null, {});
  }
};
